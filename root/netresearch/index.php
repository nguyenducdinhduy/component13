<?php $pageid="netresearch";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="c-mainvisual">
    <div class="l-container1">
        <h1><span>Internet Reseach</span>ネットリサーチ</h1>
    </div>
</div>
<div class="c-breadcrumb">
    <div class="l-container1">
        <p>
            <a href="/">Cross Marketing ホーム</a>
            <a href="#">アンケート調査</a>
            <span>ネットリサーチ</span></p>
    </div>
</div>
<div class="l-container1">
    <div class="l-container1__content">
        <div class="p-netresearch p-netresearch1">
            <h2 class="c-title2">ネットリサーチとは</h2>
            <img class="p-netresearch__img" src="/assets/img/netresearch/img.png" alt="ネットリサーチとは">
            <p class="p-netresearch__text">
                インターネット上で行う様々な調査のことで、「ネットリサーチ」「Webアンケート」とも言います。あらかじめ登録された「パネル」または「モニター（会員）」と呼ばれるアンケート回答者へ、目的に合わせて設問を設定し（調査設計）、メールや専用システムなどWebを利用して設問を配信・回収する仕組みです。インタラクティブ（双方向的）なコミュニケーションが、可能になるインターネットの性質を最大限に活用したアンケートシステムです。
                <br>
                <br> 大量のサンプル数を対象にした全国規模のアンケートも、ごく一部の地域など属性・対象を細かく絞り込んだアンケートも、ニーズに合わせて設問やパネルなどを適切に設定できます。なおかつ、システムにより事前設定することで矛盾の生じる回答などを弾いて集計できるので、有効回答数が効率的に得られます。集計は専用システムで自動的に行われ、詳細な分析が必要な場合は専門的な知識を持つ分析担当者（リサーチャー）が目的に合わせてより高度な分析を行った上でレポートにまとめます。「訪問調査」や「郵送調査」、「電話調査」など、従来からある調査・アンケート手法に比べ、インターネットリサーチは、スピーディかつ低コストで自由度が高いことが特徴のアンケート手法です。インターネットリサーチにより出されるデータの質は、登録されているパネルの質と、リサーチャーの質に大きく左右されます。大人数かつ信頼性の高いパネルが多く、専門的で経験豊富なリサーチャーが担当すれば、インターネットリサーチの質は高くなります。
            </p>
        </div>
        <div class="p-netresearch p-netresearch2">
            <h2 class="c-title2">クロス・マーケティングのネットリサーチの特長</h2>
            <p class="p-netresearch__text">
                マーケティング・リサーチのプロフェッショナル企業であるクロス・マーケティングは、登録されているアクティブなパネル数は、提携会社の登録パネルも加え、国内最大級の約420万人。大量またはニーズに合わせたアンケート回答とデータを得ることが可能です。複雑な調査設計も可能にする自由度の高い調査設計、各種分野に精通した専門性の高い専属リサーチャーなど、サポート体制も充実しています。市場ニーズを明確化し、お客様の課題を解決に導くための、最高品質のインターネットリサーチを提供しております。
            </p>
            <ul class="c-list8">
                <li>
                    <a href="#1"><img src="/assets/img/netresearch/icon1.png" alt="アクティブパネル420万人以上"></a>
                    <p>アクティブパネル<br>420万人以上</p>
                </li>
                <li>
                    <a href="#2"><img src="/assets/img/netresearch/icon2.png" alt="柔軟な調査画面設計"></a>
                    <p>柔軟な<br class="pc-only">調査画面設計</p>
                </li>
                <li>
                    <a href="#3"><img src="/assets/img/netresearch/icon3.png" alt="無料集計ツール"></a>
                    <p>無料<br class="pc-only">集計ツール</p>
                </li>
            </ul>
        </div>
        <div class="p-netresearch p-netresearch3">
            <h2 id="1" class="c-title5">アクティブパネル420万人以上</h2>
            <p class="p-netresearch__text">
                アクティブなパネルは国内リサーチ会社で最大規模の約420万人。そのすべてに対して、お客様のニーズに合わせた対象者へのアンケート配信が可能です。パネルは、基本属性（性別、年齢、住所など）だけでなく、シニア/携帯電話利用/自動車保有/化粧品利用といったライフスタイルや購買行動などで約20のカテゴリーに分類されています。属性を問わない大規模な調査から、対象者を限定した調査まで、規模を問わず対応できます。さらに、「Ponta」リサーチ会員など、弊社と提携する会社のパネルも調査に利用できます。
                <br>
                <br> 規模の大小問わず、あらゆるインターネットリサーチのニーズに応えます。ぜひ、ご相談・ご依頼ください。
            </p>
            <h3 class="p-netresearch__title">世界最大規模約420万人の大規模アクティブパネル</h3>
            <p class="p-netresearch__chart">
                <img class="pc-only" src="/assets/img/netresearch/chart.png" alt="世界最大規模約420万人の大規模アクティブパネル">
                <a href="/assets/img/netresearch/chart_sp.png" rel="lightbox" class="sp-only"><img src="/assets/img/netresearch/chart_sp.png" alt="世界最大規模約420万人の大規模アクティブパネル"></a>
            </p>
            <h4 class="p-netresearch__note">スペシャリティパネル</h4>
        </div>
        <div class="p-netresearch p-netresearch4">
            <h2 id="2" class="c-title5">柔軟な画面設計</h2>
            <p class="p-netresearch__text">
                お客様のニーズに合わせて、自由度の高い、オーダーメイドのインターネットリサーチ用アンケート画面を作成いたします。基本的な調査から複雑な調査設計まで柔軟に対応できます。動画配信やFlashなどを使用する特殊なアンケート調査も可能です。
            </p>
            <h3 class="c-title3">アンケート画面サンプル</h3>
            <p class="p-netresearch__text">基本的なアンケート画面の一例です。画面クリックで拡大します。</p>
            <div class="c-img1">
                <div class="c-img1__item">
                    <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img1.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img1.png" alt="基本設問"></a></div>
                    <h4 class="c-img1__text">基本設問</h4>
                </div>
                <div class="c-img1__item">
                    <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img2.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img2.png" alt="マトリクス設問"></a></div>
                    <h4 class="c-img1__text">マトリクス設問</h4>
                </div>
                <div class="c-img1__item">
                    <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img3.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img3.png" alt="設問項目ランダム表示"></a></div>
                    <h4 class="c-img1__text">設問項目ランダム表示</h4>
                </div>
                <div class="c-img1__item">
                    <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img4.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img4.png" alt="設問項目の再表示"></a></div>
                    <h4 class="c-img1__text">設問項目の再表示</h4>
                </div>
                <div class="c-img1__item">
                    <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img5.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img5.png" alt="回答による問題生成"></a></div>
                    <h4 class="c-img1__text">回答による問題生成</h4>
                </div>
            </div>
            <h3 class="c-title3">オプションアンケート「Webシェルフ」</h3>
            <p class="p-netresearch__text">
                インターネットリサーチ用のアンケート画面上に、架空の商品棚（Webシェルフ）を再現します。商品の種類・性質を問わず、お客様のご要望に応じた調査ができます。自動販売機やアイスクリームの冷凍ボックスなど、特殊な商品の棚もWeb上で再現し、商品パッケージに対する評価アンケートなどを視覚的に集計・分析します。商品の棚割、価格設定、POPの設置も簡単に入れ替え可能です。
            </p>
            <ul class="c-img2">
                <li>
                    <img src="/assets/img/top/maketing_img6.png" alt="お菓子">
                    <p>お菓子</p>
                </li>
                <li>
                    <img src="/assets/img/top/maketing_img7.png" alt="自動販売機">
                    <p>自動販売機</p>
                </li>
                <li>
                    <img src="/assets/img/top/maketing_img8.png" alt="冷凍ボックス">
                    <p>冷凍ボックス</p>
                </li>
                <li>
                    <img src="/assets/img/top/maketing_img9.png" alt="ドラッグストア">
                    <p>ドラッグストア</p>
                </li>
                <li>
                    <img src="/assets/img/top/maketing_img10.png" alt="ペットボトル">
                    <p>ペットボトル</p>
                </li>
            </ul>
        </div>
        <div class="p-netresearch p-netresearch5">
            <div class="p-netresearch__box">
                <h2 id="3" class="c-title5">無料アンケート集計ツール</h2>
                <div>
                    <img src="/assets/img/netresearch/img4.png" alt="無料アンケート集計ツール">
                    <p class="p-netresearch__text">
                        調査をご依頼いただいたお客様に、弊社独自で開発した無料の集計ツール「Cross Finder」を提供しています。調査で収集したアンケートデータの、質問加工/クロス集計/検定・多変量解析/レポート出力が非常に簡単です。
                    </p>
                </div>
            </div>
            <div class="c-list9">
                <div class="c-list9__box">
                    <div class="c-list9__fll">
                        <div class="c-list9__tt">
                            <figure><img src="/assets/img/netresearch/check1.png" alt="Check 01"></figure>
                            <h3>処理速度を<br class="pc-only">大幅改善！</h3>
                        </div>
                        <p class="c-list9__text">
                            データ取り込み速度を大幅に改善。
                            <br>集計表やグラフ作成も、さらに早く美しく。
                        </p>
                    </div>
                    <div class="c-list9__flr">
                        <figure><img src="/assets/img/netresearch/img1.png" alt="処理速度を大幅改善！"></figure>
                        <h4>データ読み込み速度の比較</h4>
                        <p class="c-list9__note">
                            ※データは1,000サンプル、200カラムでの比較参考値です。
                        </p>
                    </div>
                </div>
                <div class="c-list9__box">
                    <div class="c-list9__fll">
                        <div class="c-list9__tt">
                            <figure><img src="/assets/img/netresearch/check2.png" alt="Check 02"></figure>
                            <h3>マニュアルなしで<br class="pc-only">ラクラク集計！</h3>
                        </div>
                        <p class="c-list9__text">
                            初めての方でも分かりやすい画面で、面倒な設定も不要！
                            <br>さらに使いやすく生まれ変わりました。
                        </p>
                    </div>
                    <div class="c-list9__flr">
                        <figure><img src="/assets/img/netresearch/img2.png" alt="マニュアルなしでラクラク集計！"></figure>
                    </div>
                </div>
                <div class="c-list9__box">
                    <div class="c-list9__fll">
                        <div class="c-list9__tt">
                            <figure><img src="/assets/img/netresearch/check3.png" alt="Check 03"></figure>
                            <h3>検定・多変量解析<br class="pc-only">機能の追加！</h3>
                        </div>
                        <p class="c-list9__text">
                            検定・多変量解析機能を標準装備。
                            <br>t検定やz検定、コレスポンデンス分析などがこれ1本で実現できます。
                        </p>
                    </div>
                    <div class="c-list9__flr">
                        <figure><img src="/assets/img/netresearch/img2.png" alt="検定・多変量解析機能の追加！"></figure>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-netresearch p-netresearch6">
            <h2 class="c-title2">ネットリサーチの料金・価格</h2>
            <table class="c-table1 pc-only">
                <tr>
                    <th>設問数</th>
                    <th colspan="5">サンプル数</th>
                </tr>
                <tr>
                    <th>パネル</th>
                    <th>100</th>
                    <th>300</th>
                    <th>500</th>
                    <th>700</th>
                    <th>1,000</th>
                </tr>
                <tr>
                    <th>10問まで</th>
                    <td>100,000円</td>
                    <td>140,000円</td>
                    <td>180,000円</td>
                    <td>220,000円</td>
                    <td>280,000円</td>
                </tr>
                <tr>
                    <th>20問まで</th>
                    <td>174,000円</td>
                    <td>230,000円</td>
                    <td>292,000円</td>
                    <td>360,000円</td>
                    <td>458,000円</td>
                </tr>
                <tr>
                    <th>30問まで</th>
                    <td>268,000円</td>
                    <td>340,000円</td>
                    <td>424,000円</td>
                    <td>520,000円</td>
                    <td>656,000円</td>
                </tr>
                <tr>
                    <th>40問まで</th>
                    <td>342,000円</td>
                    <td>430,000円</td>
                    <td>536,000円</td>
                    <td>660,000円</td>
                    <td>834,000円</td>
                </tr>
                <tr>
                    <th>50問まで</th>
                    <td>436,000円</td>
                    <td>540,000円</td>
                    <td>668,000円</td>
                    <td>820,000円</td>
                    <td>1,032,000円</td>
                </tr>
            </table>
            <p class="sp-only p-netresearch__table">
                <a href="/assets/img/netresearch/table.png" rel="lightbox" class="sp-only"><img src="/assets/img/netresearch/table.png" alt="世界最大規模約420万人の大規模アクティブパネル"></a>
            </p>
            <ul class="c-table1__note">
                <li>※上記は基本料金、画面作成・プログラミング料金、データ作成料金を合算した本調査料金です。</li>
                <li>※本調査対象の条件によって、別途スクリーニング料金が追加となります。<br>詳しくは弊社営業担当までお問い合わせください。</li>
            </ul>
        </div>
        <div class="p-netresearch p-netresearch7">
            <h2 class="c-title4"><span>Related Contents</span>／ 関連コンテンツ</h2>
            <ul class="c-list6">
                <li>
                    <p class="c-list6__img"><a href=""><img src="/assets/img/common/content_img1.png" alt="パネル"></a></p>
                    <p class="c-list6__txt">パネル</p>
                </li>
                <li>
                    <p class="c-list6__img"><a href=""><img src="/assets/img/common/content_img2.png" alt="スペシャリティパネル"></a></p>
                    <p class="c-list6__txt">スペシャリティパネル</p>
                </li>
                <li>
                    <p class="c-list6__img"><a href=""><img src="/assets/img/common/content_img3.png" alt="よくあるご質問"></a></p>
                    <p class="c-list6__txt">よくあるご質問</p>
                </li>
            </ul>
        </div>
        <div class="p-netresearch p-netresearch8">
            <h2 class="c-title4"><span>Related Column</span>／ 関連コラム</h2>
            <ul class="c-list7">
                <li>
                    <a href=""><img src="/assets/img/common/column_img1.png" alt="ネットリサーチにおけるスマホとPCの差異と今後の展望"></a>
                    <p class="c-list7__txt1">ネットリサーチにおけるスマホとPCの差異と今後の展望</p>
                    <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
                </li>
                <li>
                    <a href=""><img src="/assets/img/common/column_img2.png" alt="ネットリサーチは「パネル」と「リサーチャー」によって質が決まる"></a>
                    <p class="c-list7__txt1">ネットリサーチは「パネル」と「リサーチャー」によって質が決まる</p>
                    <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
                </li>
                <li>
                    <a href=""><img src="/assets/img/common/column_img3.png" alt="スマートフォン利用者を意識した調査をしよう"></a>
                    <p class="c-list7__txt1">スマートフォン利用者を意識した調査をしよう</p>
                    <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
                </li>
                <li>
                    <a href=""><img src="/assets/img/common/column_img4.png" alt="あらためて知っておきたい ネットリサーチのメリットとデメリット"></a>
                    <p class="c-list7__txt1">あらためて知っておきたい ネットリサーチのメリットとデメリット</p>
                    <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="l-container1__sidebar">
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/side.php'); ?>
    </div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>