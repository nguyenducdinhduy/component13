<!DOCTYPE html>
<html lang="ja" id="<?php echo $pageid; ?>">

<head>
    <meta charset="UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <?php include('meta.php'); ?>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:100,300,400,500,700|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link href="/assets/css/style.min.css" rel="stylesheet">
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>

<body class="page-<?php echo $pageid; ?>">
    <div id="wrapper">
        <header class="c-header">
            <div class="c-header__content">
                <div class="c-header__logo">
                    <a href="/"><img src="/assets/img/common/logo_dark.png" alt="Cross Marketing"></a>
                </div>
                <div class="c-header__option">
                    <a href="#" class="lag">English</a>
                    <p class="c-scrool"><button class="c-menu"><span></span></button></p>
                </div>
                <div class="c-navi3">
                    <div class="c-navi3__item c-navi3__popup">
                        <p><img src="/assets/img/icon/icon_login.svg" height="20"><span class="c-navi3__txt">ログイン</span></p>
                        <div class="c-navi3__box">
                            <div class="c-navi3__login">
                                <form>
                                    <label>お客様ID(メールアドレス)</label>
                                    <input type="text" placeholder="半角英数">
                                    <label>パスワード</label>
                                    <input type="password" placeholder="半角英数・記号">
                                    <div class="c-submit"><input type="submit" value="ログインする"></div>
                                </form>
                                <p><a href="/">パスワードをお忘れの方はこちら</a></p>
                                <p><a href="/">無料会員登録(ID・パスワードをお持ちでないお客様)</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="c-navi3__item c-navi3__bnt">
                        <p><a href="https://www.google.com/"><img src="/assets/img/icon/icon_net-research.svg" height="20"></a><span class="c-navi3__txt">調査相談</span></p>
                    </div>
                    <div class="c-navi3__item c-navi3__bnt">
                        <p><a href="#"><img src="/assets/img/icon/icon_estimate.svg" height="20"></a><span class="c-navi3__txt">お見積もり</span></p>
                    </div>
                    <div class="c-navi3__item c-navi3__bnt">
                        <p><a href="#"><img src="/assets/img/icon/icon_document.svg" height="20"></a><span class="c-navi3__txt">資料請求</span></p>
                    </div>
                    <div class="c-navi3__item  c-navi3__popup">
                        <p><img src="/assets/img/icon/icon_contact.svg" height="20"><span class="c-navi3__txt">お問い<br>合わせ</span></p>
                        <div class="c-navi3__box">
                            <div class="c-navi3__contact">
                                <p>フリーダイヤルでのお問い合わせ</p>
                                <div class="c-navi3__tel"><a href="tel:0120198022"></a>0120-198-022</div>
                                <div class="c-bnt1 c-bnt1__white"><a href="#"><span>お問い合わせフォーム</span></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="c-navi3__item  c-navi3__popup">
                        <p><img src="/assets/img/icon/icon_search.svg" height="20"><span class="c-navi3__txt">検索</span></p>
                        <div class="c-navi3__box">
                            <div class="c-navi3__search">
                                <form>
                                    <input type="text" placeholder="キーワード検索">
                                    <input type="submit" value="">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-header__navi">
                    <nav class="c-navi">
                        <ul>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/netresearch/"></a>クロス・マーケティング ホーム</p>
                            </li>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/netresearch/"></a>ネットリサーチ</p>
                            </li>
                            <li class="c-navi__sub">
                                <p class="c-navi__item"><a href="#"></a>アンケート調査</p>
                                <div class="c-navi__child">
                                    <div class="c-navi__child__title">アンケート調査</div>
                                    <div class="l-container">
                                        <div>
                                            <p><a href="/netresearch/">アンケート調査TOP</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">デプスインタビュー</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">グループインタビュー</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">インサイト発見サービス</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">会場調査</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">Webシェルフ</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">ホームユーステスト</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">アイトラッキング</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">店頭調査</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">アンケートASPサービス</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">Webリクルート</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">海外調査</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">ミステリーショッパー<br class="sp-only">（覆面調査）</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">データ集計・変換</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">学術調査</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="pc-only">
                                <p class="c-navi__item"><a href="/"></a>Originality</p>
                            </li>
                            <li class="sp-only c-navi__sub">
                                <p class="c-navi__item"><a href="#"></a>ソリューション</p>
                                <div class="c-navi__child">
                                    <div class="c-navi__child__title">ソリューション</div>
                                    <div class="l-container">
                                        <div>
                                            <p><a href="/netresearch/">ソリューションTOP</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">BI Cross</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">セルフ型アンケートツール<br class="sp-only">「QiQUMO」</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">RDIT（アールディット）</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">PerceptionR</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">社員満足度調査</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">China Pulse</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">ニューロマーケティング</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">サピエンス消費</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">インスタグラマー view</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">Cross Trace</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="c-navi__sub">
                                <p class="c-navi__item"><a href="#"></a>統計・分析手法</p>
                                <div class="c-navi__child">
                                    <div class="c-navi__child__title">統計・分析手法</div>
                                    <div class="l-container">
                                        <div>
                                            <p><a href="#">統計・分析手法TOP</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">重回帰分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">クラスター分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">コレスポンデンス分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">因子分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">共分散構造分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">PSM分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">ディシジョンツリー分析</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">ポートフォリオ分析</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="c-navi__sub">
                                <p class="c-navi__item"><a href="#"></a>パネル</p>
                                <div class="c-navi__child">
                                    <div class="c-navi__child__title">パネル</div>
                                    <div class="l-container">
                                        <div>
                                            <p><a href="#">パネルTOP</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">スペシャリティパネル</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">パネル</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="c-navi__sub">
                                <p class="c-navi__item"><a href="#"></a>無料調査レポート</p>
                                <div class="c-navi__child">
                                    <div class="c-navi__child__title">無料調査レポート</div>
                                    <div class="l-container">
                                        <div>
                                            <p><a href="#">無料調査レポートTOP</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">くろめがね</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">無料調査レポート</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="pc-only">
                                <p class="c-navi__item"><a href="#"></a>Cross Column</p>
                            </li>
                            <li class="sp-only c-navi__sub">
                                <p class="c-navi__item"><a href="#"></a>コラム</p>
                                <div class="c-navi__child">
                                    <div class="c-navi__child__title">コラム</div>
                                    <div class="l-container">
                                        <div>
                                            <p><a href="#">コラムTOP</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">マーケティングコラム</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">宣伝会議×CM<br>「Future Marketing」</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="pc-only">
                                <p class="c-navi__item"><a href="#"></a>会社概要</p>
                            </li>
                            <li class="pc-only">
                                <p class="c-navi__item"><a href="/"></a>お問い合わせ</p>
                            </li>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/"></a>セミナー</p>
                            </li>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/"></a>よくあるご質問</p>
                            </li>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/"></a>無料集計ツール「Cross Finder」</p>
                            </li>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/"></a>無料トライアル調査</p>
                            </li>
                            <li class="sp-only">
                                <p class="c-navi__item"><a href="/"></a>マーケティング用語集</p>
                            </li>
                            <li class="c-navi__last c-navi__sub sp-only">
                                <div class="c-navi__child">
                                    <div class="l-container">
                                        <div>
                                            <p><a href="#">会社概要</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">採用情報</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">お知らせ</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">品質管理ポリシー</a></p>
                                        </div>
                                        <div>
                                            <p><a href="#">個人情報保護方針</a></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <div class="c-navi4 sp-only">
                        <div class="l-container">
                            <ul>
                                <li>
                                    <img src="/assets/img/icon/icon_net-research.svg" alt="調査依頼・相談">
                                    <a href="#">調査依頼・相談</a>
                                </li>
                                <li>
                                    <img src="/assets/img/icon/icon_estimate.svg" alt="お見積もり">
                                    <a href="#">お見積もり</a>
                                </li>
                                <li>
                                    <img src="/assets/img/icon/icon_document.svg" alt="資料請求">
                                    <a href="#">資料請求</a>
                                </li>
                                <li>
                                    <img src="/assets/img/icon/icon_contact.svg" alt="お問い合わせ">
                                    <a href="#">お問い合わせ</a>
                                </li>
                                <li class="tel">
                                    <p>フリーダイヤルでのお問い合わせ</p>
                                    <div><a href="tel:0120198022">0120-198-022</a></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main>