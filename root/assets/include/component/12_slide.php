<?php /*========================================
slide
================================================*/ ?>
<div class="c-dev-title1">slide</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-slide</div>

<div class="c-slide">
    <div class="c-slide__content">
        <div class="swiper-slide">
            <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide1.jpg);"></div>
            <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide1_sp.jpg);"></div>
            <div class="c-slide__left">
                <div class="c-slide__text">
                    <div class="c-slide__txt">
                        <div class="c-slide__sppos">
                            <h3>マーケティング・リサーチの、その先へ。<span>Data Marketing</span></h3>
                            <p>あらゆるデータを活⽤し、<br>お客様のマーケティングを⽀援。</p>
                        </div>
                        <div class="c-bnt1"><a href="#"><span>クロス・マーケティングの特長</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide2.jpg);"></div>
            <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide2_sp.jpg);"></div>
            <div class="c-slide__right">
                <div class="c-slide__text">
                    <div class="c-slide__txt">
                        <div class="c-slide__sppos">
                            <h3>マーケティング・リサーチの、その先へ。<span>Marketing Research</span></h3>
                            <p>私たちは時代変化に対応し、データ＆コンサルティングにより、<br>お客様のビジネスを成功に導いていきます。</p>
                        </div>
                        <div class="c-bnt1"><a href="#"><span>クロス・マーケティングの特長</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide3.jpg);"></div>
            <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide3_sp.jpg);"></div>
            <div class="c-slide__right">
                <div class="c-slide__text">
                    <div class="c-slide__txt">
                        <div class="c-slide__sppos">
                            <h3>マーケティング・リサーチの、その先へ。<span>Consultancy</span></h3>
                            <p>お客様のパートナーとして、リサーチ、<br>課題整理、戦略策定に至るPDCAをサポート。</p>
                        </div>
                        <div class="c-bnt1"><a href="#"><span>クロス・マーケティングの特長</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide4.jpg);"></div>
            <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide4_sp.jpg);"></div>
            <div class="c-slide__left">
                <div class="c-slide__text">
                    <div class="c-slide__txt">
                        <div class="c-slide__sppos">
                            <h3>マーケティング・リサーチの、その先へ。<span>Global</span></h3>
                            <p>世界各地の拠点と多数の海外社員からなる、<br>グローバルな調査ネットワーク。</p>
                        </div>
                        <div class="c-bnt1"><a href="#"><span>クロス・マーケティングの特長</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide5.jpg);"></div>
            <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide5_sp.jpg);"></div>
            <div class="c-slide__left">
                <div class="c-slide__text">
                    <div class="c-slide__txt">
                        <div class="c-slide__sppos">
                            <h3>マーケティング・リサーチの、その先へ。<span>Marketing Intelligence Company</span></h3>
                            <p>私たちは時代変化に対応し、データ＆コンサルティングにより、<br>お客様のビジネスを成功に導いていきます。」</p>
                        </div>
                        <div class="c-bnt1"><a href="#"><span>クロス・マーケティングの特長</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-slide__news">
        <div class="c-slide__item">
            <p class="c-slide__item__tit">お知らせ</p>
            <div class="c-slide__item__content">
                <p>2019.02.26<span>プレスリリース</span></p>
                <p><a href="#">賃貸物件の部屋探しに関する調査</a></p>
            </div>
        </div>
        <div class="c-slide__news__bnt">
            <div class="c-slide__pag">
                <div class="c-number"></div>
            </div>
            <div class="c-slide__total"></div>
            <div class="c-slide__list"><a href="#"><img src="/assets/img/icon/icon_list2.svg"></a></div>
        </div>
    </div>
</div>