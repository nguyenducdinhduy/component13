<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-img1</div>
<div class="l-container2">
    <div class="c-img1">
        <div class="c-img1__item">
            <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img1.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img1.png" alt="基本設問"></a></div>
            <h4 class="c-img1__text">基本設問</h4>
        </div>
        <div class="c-img1__item">
            <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img2.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img2.png" alt="マトリクス設問"></a></div>
            <h4 class="c-img1__text">マトリクス設問</h4>
        </div>
        <div class="c-img1__item">
            <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img3.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img3.png" alt="設問項目ランダム表示"></a></div>
            <h4 class="c-img1__text">設問項目ランダム表示</h4>
        </div>
        <div class="c-img1__item">
            <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img4.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img4.png" alt="設問項目の再表示"></a></div>
            <h4 class="c-img1__text">設問項目の再表示</h4>
        </div>
        <div class="c-img1__item">
            <div class="c-img1__lightbox"><a href="/assets/img/top/maketing_img5.png" rel="lightbox[1]"><img src="/assets/img/top/maketing_img5.png" alt="回答による問題生成"></a></div>
            <h4 class="c-img1__text">回答による問題生成</h4>
        </div>
    </div>
</div>
<div class="c-dev-title2">c-img2</div>
<div class="l-container2">
    <ul class="c-img2">
        <li>
            <img src="/assets/img/top/maketing_img6.png" alt="お菓子">
            <p>お菓子</p>
        </li>
        <li>
            <img src="/assets/img/top/maketing_img7.png" alt="自動販売機">
            <p>自動販売機</p>
        </li>
        <li>
            <img src="/assets/img/top/maketing_img8.png" alt="冷凍ボックス">
            <p>冷凍ボックス</p>
        </li>
        <li>
            <img src="/assets/img/top/maketing_img9.png" alt="ドラッグストア">
            <p>ドラッグストア</p>
        </li>
        <li>
            <img src="/assets/img/top/maketing_img10.png" alt="ペットボトル">
            <p>ペットボトル</p>
        </li>
    </ul>
</div>