<?php /*========================================
table
================================================*/ ?>
<div class="c-dev-title1">table</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table1</div>
<div class="l-container2">
    <div class="c-zoom">
        <table class="c-table1">
            <tr>
                <th>設問数</th>
                <th colspan="5">サンプル数</th>
            </tr>
            <tr>
                <th>パネル</th>
                <th>100</th>
                <th>300</th>
                <th>500</th>
                <th>700</th>
                <th>1,000</th>
            </tr>
            <tr>
                <th>10問まで</th>
                <td>100,000円</td>
                <td>140,000円</td>
                <td>180,000円</td>
                <td>220,000円</td>
                <td>280,000円</td>
            </tr>
            <tr>
                <th>20問まで</th>
                <td>174,000円</td>
                <td>230,000円</td>
                <td>292,000円</td>
                <td>360,000円</td>
                <td>458,000円</td>
            </tr>
            <tr>
                <th>30問まで</th>
                <td>268,000円</td>
                <td>340,000円</td>
                <td>424,000円</td>
                <td>520,000円</td>
                <td>656,000円</td>
            </tr>
            <tr>
                <th>40問まで</th>
                <td>342,000円</td>
                <td>430,000円</td>
                <td>536,000円</td>
                <td>660,000円</td>
                <td>834,000円</td>
            </tr>
            <tr>
                <th>50問まで</th>
                <td>436,000円</td>
                <td>540,000円</td>
                <td>668,000円</td>
                <td>820,000円</td>
                <td>1,032,000円</td>
            </tr>
        </table>
    </div>
</div>