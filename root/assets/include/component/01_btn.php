<?php /*========================================
btn
================================================*/ ?>
<div class="c-dev-title1">btn</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-bnt</div>
<div class="l-container">
    <ul class="c-bnt c-bnt__line">
        <li><a href="#"><span>クロス・マーケティングの特長</span></a></li>
    </ul>
    <br><br>
    <ul class="c-bnt c-bnt__line">
        <li><a href="#"><span>無料調査レポート一覧</span></a></li>
        <li><a href="#"><span>くろめがね一覧</span></a></li>
    </ul>
    <br><br>
    <ul class="c-bnt">
        <li><a href="#"><span>ネットリサーチ</span></a></li>
        <li><a href="#"><span>アンケート調査から探す</span></a></li>
        <li><a href="#"><span>ソリューションから探す</span></a></li>
        <li><a href="#"><span>統計・分析手法から探す</span></a></li>
    </ul>
</div>