
var width = window.innerWidth || document.documentElement.clientWidth;
var n = $(".c-slide .swiper-slide").length;
for (i = 1; i <= n; i++) { $('.c-number').append('<div class="item">' + i + '</div>'); }
$('.c-slide__total').append('/' + n);
//$(window).resize(function () { location.reload(); });
// $('.page-top .c-header').addClass("c-header__light");
(function ($) {
    $.fn.tile = function (columns) {
        var tiles, max, c, h, last = this.length - 1, s;
        if (!columns) columns = this.length;
        this.each(function () {
            s = this.style;
            if (s.removeProperty) s.removeProperty("height");
            if (s.removeAttribute) s.removeAttribute("height");
        });
        return this.each(function (i) {
            c = i % columns;
            if (c == 0) tiles = [];
            tiles[c] = $(this);
            h = tiles[c].height();
            if (c == 0 || h > max) max = h;
            if (i == last || c == columns - 1)
                $.each(tiles, function () { this.height(max); });
        });
    };
})(jQuery);

$(window).on("load resize", function (e) {
    width = window.innerWidth || document.documentElement.clientWidth;
    //$(".c-zoom").children().css("zoom", $(".c-zoom").width() / 700);
    if (width < 768) {
        $('.c-list1').slick({
            centerMode: true,
            slidesToShow: 1,
            arrows: false,
        });
        $('.c-list5').slick({
            centerMode: true,
            slidesToShow: 1,
            arrows: false,
        });
    }

    $('.c-list2__tit').tile(4);

});

$('.c-slide__content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    centerMode: true,
    centerPadding: '0px',
    // fade: true,
    asNavFor: '.c-number',
    speed: 1000,
    autoplay: false,
    autoplaySpeed: 5600,
    pauseOnFocus: false,
    pauseOnHover: false,
    pauseOnDotsHover: false,
    touchMove: false,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                centerPadding: '0',
            }
        }
    ]
});


$('.c-number').slick({
    slidesToShow: 1,
    slidesToScroll: 0,
    asNavFor: '.c-slide__content',
    // dots: true,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '0',


});
// $(".slick-arrow").click(function () {
//     $(".slick-arrow").css("pointer-events", "none");
//     setTimeout(function () {
//         $(".slick-arrow").removeAttr('style');
//     }, 1000);
// });
$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
$('.c-navi3__popup > p').click(function (event) {
    if (width < 768) {
        if ($(this).hasClass('opened')) {
            $('.c-navi3__popup').children('div').slideUp(300);
            $('.c-navi3__popup > p').removeClass('opened');
        } else {
            if ($('.c-navi3__popup > p').hasClass('opened')) {
                $('.c-navi3__popup').children('div').slideUp(300);
                $(this).next('div').delay(300).slideDown(300);
            }
            else {
                $(this).next('div').slideDown(300);
            }
            $('.c-navi3__popup > p').removeClass('opened');
            $(this).addClass('opened');
        }
    }
});

$(function () {
    $(".c-navi__child").hide();
    $(".c-navi .c-navi__sub").hover(function () {
        if (width > 768) {
            $(this).addClass("hover");
            $(".c-navi__child", this).stop(true, true).slideDown(200);
            var i = 0;
            var rows = $(this).find(".l-container div");
            huhu();
            function huhu() {
                if (i < rows.length) {
                    $(rows[i]).addClass('active');
                    i++;
                    setTimeout(huhu, 20);
                }
            }
        }
    },
        function () {
            if (width > 768) {
                $(this).removeClass("hover");
                $(".c-navi__child", this).stop(true, true).slideUp(200);
                $(this).find(".l-container div").removeClass("active");
            }

        });
});
var scrl_pos;
$('.c-menu').on("click", function () {
    if (width < 768) {
        $(this).toggleClass('on');
        if ($("header").hasClass('active')) {
            $('header,body,.c-header__navi').removeClass("active");
            $('.c-scrool').removeClass("on");
            console.log(scrl_pos)
            $("html, body").animate({ scrollTop: scrl_pos }, 0);
        }
        else {
            scrl_pos = $(window).scrollTop();
            $('.c-scrool').addClass("on");
            $('header,body,.c-header__navi').addClass("active");
            $('.c-navi3').removeAttr("style").removeClass("scrool");
            $('.c-header__navi').removeClass("notscrool");
            $('body').css({ 'top': -scrl_pos });
        }

    }
});

$('.c-navi__sub').on("click", function () {
    if (width < 768) {
        $('.c-navi__sub  .c-navi__child').slideUp();
        if ($(this).hasClass('on')) {
            $(this).children('.c-navi__child').slideUp();
            $('.c-navi__sub').removeClass('on');
        } else {
            $(this).children('.c-navi__child').slideDown();
            $('.c-navi__sub').removeClass('on');
            $(this).addClass('on');
        }
    }
});

$(document).ready(function () {
    if (width < 768) {
        var flag = false;
        $(window).scroll(function () {
            if ($(this).scrollTop() > 400) {
                if (flag == false) {
                    flag = true;
                    $('.c-scrool,.c-navi3,header').addClass("scrool");
                    $('.c-navi3').removeClass('notscrool');
                    $('.c-scrool').stop().animate({
                        'right': '7px'
                    }, 400);
                    $('.c-navi3').stop().animate({
                        'bottom': '10px'
                    }, 400);

                }
            } else {
                if (flag) {
                    flag = false;
                    if (!$(".c-scrool").hasClass('on')) {
                        $('.c-scrool').stop().animate({
                            'right': '-500px'
                        }, 200);
                        $('.c-navi3').stop().animate({
                            'bottom': '-500px'
                        }, 200);
                        setTimeout(function () {
                            $('.c-navi3,.c-scrool,header').removeClass("scrool");
                            $('.c-navi3,.c-header__navi').addClass('notscrool');
                        }, 100);
                    }
                }
            }
        });
    }
});

