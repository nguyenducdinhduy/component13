</main>
<footer class="c-footer">
    <div class="c-navi4">
        <div class="l-container">
            <ul>
                <li>
                    <img src="/assets/img/icon/icon_net-research.svg" alt="調査依頼・相談">
                    <a href="#">調査依頼・相談</a>
                </li>
                <li>
                    <img src="/assets/img/icon/icon_estimate.svg" alt="お見積もり">
                    <a href="#">お見積もり</a>
                </li>
                <li>
                    <img src="/assets/img/icon/icon_document.svg" alt="資料請求">
                    <a href="#">資料請求</a>
                </li>
                <li>
                    <img src="/assets/img/icon/icon_contact.svg" alt="お問い合わせ">
                    <a href="#">お問い合わせ</a>
                </li>
                <li class="tel">
                    <p>フリーダイヤルでのお問い合わせ</p>
                    <div><a href="tel:0120198022">0120-198-022</a></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="c-footer__brc">
        <div class="l-container">
            <p>Cross Marketing ホーム</p>
        </div>
    </div>
    <div class="c-footer__navi">
        <div class="l-container">
            <ul class="c-footer__menu">
                <li><a href="#">クロス・マーケティング ホーム</a></li>
                <li><a href="/netresearch/">ネットリサーチ</a></li>
                <li>
                    <a href="#">アンケート調査</a>
                    <ul>
                        <li><a href="#">グループインタビュー</a></li>
                        <li><a href="#">デプスインタビュー</a></li>
                        <li><a href="#">会場調査</a></li>
                        <li><a href="#">インサイト発見サービス</a></li>
                        <li><a href="#">ホームユーステスト</a></li>
                        <li><a href="#">Webシェルフ</a></li>
                        <li><a href="#">店頭調査</a></li>
                        <li><a href="#">アイトラッキング</a></li>
                        <li><a href="#">Webリクルート</a></li>
                        <li><a href="#">アンケートASPサービス</a></li>
                        <li><a href="#">ミステリーショッパー<br class="sp-only">（覆面調査）</a></li>
                        <li><a href="#">海外調査</a></li>
                        <li><a href="#">学術調査</a></li>
                        <li><a href="#">データ集計・変換</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="c-footer__menu">
                <li>
                    <a href="#">ソリューション</a>
                    <ul>
                        <li><a href="#">セルフ型アンケートツール「QiQUMO」</a></li>
                        <li><a href="#">BI Cross</a></li>
                        <li><a href="#">PerceptionR</a></li>
                        <li><a href="#">RDIT（アールディット）</a></li>
                        <li><a href="#">China Pulse</a></li>
                        <li><a href="#">社員満足度調査</a></li>
                        <li><a href="#">サピエンス消費</a></li>
                        <li><a href="#">ニューロマーケティング</a></li>
                        <li><a href="#">Cross Trace</a></li>
                        <li><a href="#">インスタグラマー view</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">統計・分析手法</a>
                    <ul>
                        <li><a href="#">クラスター分析</a></li>
                        <li><a href="#">重回帰分析</a></li>
                        <li><a href="#">因子分析</a></li>
                        <li><a href="#">コレスポンデンス分析</a></li>
                        <li><a href="#">PSM分析</a></li>
                        <li><a href="#">共分散構造分析</a></li>
                        <li><a href="#">ポートフォリオ分析</a></li>
                        <li><a href="#">ディシジョンツリー分析</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="c-footer__menu">
                <li>
                    <a href="#">パネル</a>
                    <ul>
                        <li><a href="#">パネル</a></li>
                        <li><a href="#">スペシャリティパネル</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">無料調査レポート</a>
                    <ul>
                        <li><a href="#">無料調査レポート</a></li>
                        <li><a href="#">くろめがね</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">コラム</a>
                    <ul>
                        <li><a href="#">宣伝会議×CM<br class="sp-only">「Future Marketing」</a></li>
                        <li><a href="#">マーケティングコラム</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">セミナー</a>
                </li>
                <li>
                    <a href="#">よくあるご質問</a>
                </li>
                <li>
                    <a href="#">無料集計ツール「Cross Finder」</a>
                </li>
                <li>
                    <a href="#">無料トライアル調査</a>
                </li>
                <li>
                    <a href="#">マーケティング用語集</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="c-footer__info">
        <div class="l-container">
            <ul>
                <li><a href="#">会社概要</a></li>
                <li><a href="#">採用情報</a></li>
                <li><a href="#">お知らせ</a></li>
                <li><a href="#">品質管理ポリシー</a></li>
                <li><a href="#">個人情報保護方針</a></li>
            </ul>
            <div class="c-footer__social">
                <p><a href="#"><img src="/assets/img/icon/linkedin-logo.svg"></a></p>
                <p><a href="#"><img src="/assets/img/icon/icon_facebook.svg"></a></p>
                <p><a href="#"><img src="/assets/img/icon/icon_twitter.svg"></a></p>
            </div>
            <div class="c-footer__logo">
                <a href="#"><img src="/assets/img/common/logo_f.png" alt="Cross Marketing"></a>
                <span>株式会社 クロス・マーケティング</span>
            </div>
            <div class="c-footer__address">
                <p><span>本社</span>〒163‒1424　<br class="sp-only">東京都新宿区西新宿3‒2‒2　<br class="sp-only">東京オペラシティタワー24F</p>
                <p><span>西日本営業所</span>〒530‒0001　<br class="sp-only">大阪府大阪市北区梅田3‒4‒5　<br class="sp-only">毎日新聞ビル4階</p>
            </div>

        </div>
    </div>
    <div class="c-footer__relate">
        <div class="l-container">
            <ul>
                <li><img src="/assets/img/common/relate1.png" alt=""></li>
                <li><img src="/assets/img/common/relate2.png" alt=""></li>
                <li><img src="/assets/img/common/relate3.png" alt=""></li>
                <li><img src="/assets/img/common/relate4.png" alt=""></li>
            </ul>
            <p class="c-footer__copyright">Copyright © Cross Marketing Inc. All Rights Reserved.</p>
        </div>
    </div>
</footer>
</div>
<script src='/assets/js/slick.min.js'></script>
<script src="/assets/js/jquery-lightbox.js"></script>
<script src="/assets/js/functions.min.js"></script>
</body>

</html>