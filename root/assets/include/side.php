<div class="c-sidebar">
    <div class="c-sidebar__menu">
        <div class="c-sidebar__tit"><a href="#">アンケート調査</a></div>
        <ul>
            <li><a href="#">アンケート調査</a></li>
            <li><a href="#">グループインタビュー</a></li>
            <li><a href="#">デプスインタビュー</a></li>
            <li><a href="#">会場調査</a></li>
            <li><a href="#">インサイト発見サービス</a></li>
            <li><a href="#">ホームユーステスト</a></li>
            <li><a href="#">Webシェルフ</a></li>
            <li><a href="#">店頭調査</a></li>
            <li><a href="#">アイトラッキング</a></li>
            <li><a href="#">Webリクルート</a></li>
            <li><a href="#">アンケートASPサービス</a></li>
            <li><a href="#">ミステリーショッパー<br>（覆面調査）</a></li>
            <li><a href="#">海外調査</a></li>
            <li><a href="#">学術調査</a></li>
            <li><a href="#">データ集計・変換</a></li>
        </ul>
    </div>

    <div class="c-sidebar__banner">
        <figure><img src="/assets/img/common/sidebar_img1.png" alt="作成から分析までワンストップで行える、カンタン・低コスト・スピーディなセルフ型アンケートツール。"></figure>
        <div>
            <p>作成から分析までワンストップで行える、カンタン・低コスト・スピーディなセルフ型アンケートツール。</p>
            <a href="#">QiQUMO（キクモ）</a>
        </div>
    </div>
    <div class="c-sidebar__banner">
        <figure><img src="/assets/img/common/sidebar_img2.png" alt="生活者理解を深め、商品開発に活かしたいマーケティング担当者向け“ネットリサーチ”入門企画。"></figure>
        <div>
            <p>“生活者理解を深め、商品開発に活かしたい”マーケティング担当者向け“ネットリサーチ”入門企画。</p>
            <a href="#">初めてのネットリサーチ デキるマーケターはデータで語る！</a>
        </div>
    </div>
</div>