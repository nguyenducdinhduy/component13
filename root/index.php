<?php $pageid="top";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="c-slide">
    <div class="c-slide__content">
        <div class="swiper-slide">
            <!-- <div class="c-slide__bgbox">
                <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide1.jpg);"></div>
                <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide1_sp.jpg);"></div>
            </div> -->
            <div class="c-slide__1">
                <div class="c-slide__txtbox">
                    <h3>マーケティング・リサーチの、その先へ。</h3>
                    <h4>Data Marketing</h4>
                    <p>あらゆるデータを活⽤し、<br>お客様のマーケティングを⽀援。</p>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <!-- <div class="c-slide__bgbox">
                <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide2.jpg);"></div>
                <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide2_sp.jpg);"></div>
            </div> -->
            <div class="c-slide__2">
                <div class="c-slide__txtbox">
                    <h3>マーケティング・リサーチの、その先へ。</h3>
                    <h4>Marketing Research</h4>
                    <p>私たちは時代変化に対応し、データ＆コンサルティングにより、<br>お客様のビジネスを成功に導いていきます。</p>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <!-- <div class="c-slide__bgbox">
                <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide3.jpg);"></div>
                <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide3_sp.jpg);"></div>
            </div> -->
            <div class="c-slide__3">
                <div class="c-slide__txtbox">
                    <h3>マーケティング・リサーチの、その先へ。</h3>
                    <h4>Consultancy</h4>
                    <p>お客様のパートナーとして、リサーチ、<br>課題整理、戦略策定に至るPDCAをサポート。</p>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <!-- <div class="c-slide__bgbox">
                <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide4.jpg);"></div>
                <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide4_sp.jpg);"></div>
            </div> -->
            <div class="c-slide__4">
                <div class="c-slide__txtbox">
                    <h3>マーケティング・リサーチの、その先へ。</h3>
                    <h4>Global</h4>
                    <p>世界各地の拠点と多数の海外社員からなる、<br>グローバルな調査ネットワーク。</p>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <!-- <div class="c-slide__bgbox">
                <div class="c-slide__bg pc-only" style="background-image: url(/assets/img/top/slide5.jpg);"></div>
                <div class="c-slide__bg sp-only" style="background-image: url(/assets/img/top/slide5_sp.jpg);"></div>
            </div> -->
            <div class="c-slide__5">
                <div class="c-slide__txtbox">
                    <h3>マーケティング・リサーチの、その先へ。</h3>
                    <h4>Marketing<br>Intelligence<br>Company</h4>
                    <p>私たちは時代変化に対応し、データ＆コンサルティングにより、<br>お客様のビジネスを成功に導いていきます。」</p>
                </div>
            </div>
        </div>
    </div>
    <div class="c-slide__news">
        <div class="c-slide__item">
            <p class="c-slide__item__tit">お知らせ</p>
            <div class="c-slide__item__content">
                <p>2019.02.26<span>プレスリリース</span></p>
                <p><a href="#">賃貸物件の部屋探しに関する調査</a></p>
            </div>
        </div>
        <div class="c-slide__news__bnt">
            <div class="c-slide__pag">
                <div class="c-number"></div>
            </div>
            <div class="c-slide__total"></div>
            <div class="c-slide__list"><a href="#"><img src="/assets/img/icon/icon_list2.svg"></a></div>
        </div>
    </div>
</div>
<!-- <div class="c-slide2">
    <div>
        <div class="slide c-slide__1">
        </div>
    </div>
    <div>
        <div class="slide c-slide__2">
        </div>
    </div>
    <div>
        <div class="slide c-slide__3">
        </div>
    </div>
    <div>
        <div class="slide c-slide__4">
        </div>
    </div>
    <div>
        <div class="slide c-slide__5">
        </div>
    </div>
</div> -->
<div class="p-top1">
    <div class="l-container">
        <h2 class="c-title1"><span class="c-title1__en">Feature</span><span class="c-title1__jp">クロス・マーケティングの特長</span></h2>
        <table class="c-list2">
            <tr>
                <td>
                    <figure><img src="/assets/img/top/feature_img1.png" alt="マーケティングリサーチ"></figure>
                    <h4>Marketing Research</h4>
                    <h3 class="c-list2__tit"><span>マーケティング<br class="pc-only">リサーチ</span></h3>
                    <p class="c-list2__text">
                        「市場」の動向だけでなく、市場の中身、顧客や満足度に至るまでを調査。データや数値のみでは計れない潜在的なニーズも察知・予測し、明確な目的と正しい手法で調査を実施することで、正確なデータをビジネスに有効活用できます。
                    </p>
                </td>
                <td>
                    <figure><img src="/assets/img/top/feature_img2.png" alt="データマーケティング"></figure>
                    <h4>Data Marketing</h4>
                    <h3 class="c-list2__tit"><span>データ<br class="pc-only">マーケティング</span></h3>
                    <p class="c-list2__text">
                        「市場」の動向だけでなく、市場の中身、顧客や満足度に至るまでを調査。データや数値のみでは計れない潜在的なニーズも察知・予測し、明確な目的と正しい手法で調査を実施することで、正確なデータをビジネスに有効活用できます。
                    </p>
                </td>
                <td>
                    <figure><img src="/assets/img/top/feature_img3.png" alt="グローバルリサーチ"></figure>
                    <h4>Global Research</h4>
                    <h3 class="c-list2__tit"><span>グローバル<br class="pc-only">リサーチ</span></h3>
                    <p class="c-list2__text">
                        クライアントの海外展開のニーズに応えるため、アジア市場を中心に海外拠点を展開。言語だけでなく、商慣習、法律、宗教など異なる市場環境に対応できるフィールドワーク＆リサーチネットワークを保有しています。
                    </p>
                </td>
                <td>
                    <figure><img src="/assets/img/top/feature_img4.png" alt="コンサルタンシー"></figure>
                    <h4>Consultancy</h4>
                    <h3 class="c-list2__tit"><span>コンサルタンシー</span></h3>
                    <p class="c-list2__text">
                        お客さまと密なコミュニケーションを図り信頼関係を築き、マーケティング課題を正確にヒアリング。お客さまの課題の背景にある市場状況、経営戦略、事業戦略、販売戦略などを理解した上で、最も最適なソリューションを提供します。
                    </p>
                </td>
            </tr>
        </table>
        <ul class="c-bnt c-bnt__line">
            <li><a href="#"><span>クロス・マーケティングの特長</span></a></li>
        </ul>
    </div>
</div>
<div class="p-top2">
    <div class="l-container">
        <h2 class="c-title1"><span class="c-title1__en">Service</span><span class="c-title1__jp">サービス検索</span></h2>
        <ul class="c-list4">
            <li>
                <div class="c-list4__img"><img src="/assets/img/top/service_img1.png" alt="マーケティング課題から探す"></div>
                <div class="c-list4__text">
                    <h3 class="c-list4__tit"><a href="#">マーケティング課題から探す</a></h3>
                    <p class="c-list4__txt">この文章はダミーのテキストです。実際の内容とは異なりますので予めご了承ください。この文章はダミーのテキストです。</p>
                </div>
            </li>
            <li>
                <div class="c-list4__img"><img src="/assets/img/top/service_img2.png" alt="マーケティングプロセスから探す"></div>
                <div class="c-list4__text">
                    <h3 class="c-list4__tit"><a href="#">マーケティングプロセスから探す</a></h3>
                    <p class="c-list4__txt">この文章はダミーのテキストです。実際の内容とは異なりますので予めご了承ください。この文章はダミーのテキストです。</p>
                </div>
            </li>
            <li>
                <div class="c-list4__img"><img src="/assets/img/top/service_img3.png" alt="初めてのネットリサーチ"></div>
                <div class="c-list4__text">
                    <h3 class="c-list4__tit"><a href="#">初めてのネットリサーチ</a></h3>
                    <p class="c-list4__txt">この文章はダミーのテキストです。実際の内容とは異なりますので予めご了承ください。この文章はダミーのテキストです。</p>
                </div>
            </li>
        </ul>
        <div class="p-top2__list">
            <ul class="c-list3">
                <li>
                    <figure><a href=""><img src="/assets/img/top/service_icon1.png" alt="QiQUMO"></a></figure>
                    <p class="c-list3__txt">セルフ型<br class="sp-only">アンケートツール</p>
                    <h3 class="c-list3__tit">QiQUMO</h3>
                </li>
                <li>
                    <figure><a href=""><img src="/assets/img/top/service_icon2.png" alt="オフラインリサーチ"></a></figure>
                    <p class="c-list3__txt">消費者の生活や<br class="sp-only">感情に密着できる</p>
                    <h3 class="c-list3__tit">オフラインリサーチ</h3>
                </li>
                <li>
                    <figure><a href=""><img src="/assets/img/top/service_icon3.png" alt="データ分析・解析"></a></figure>
                    <p class="c-list3__txt">目的や用途、<br class="sp-only">手法に合わせた</p>
                    <h3 class="c-list3__tit">データ分析・解析</h3>
                </li>
                <li>
                    <figure><a href=""><img src="/assets/img/top/service_icon4.png" alt="学術調査"></a></figure>
                    <p class="c-list3__txt">学術研究機関の研究者や<br class="sp-only">学生向け</p>
                    <h3 class="c-list3__tit">学術調査</h3>
                </li>
                <li>
                    <figure class="c-list3__br2"><a href=""><img src="/assets/img/top/service_icon5.png" alt="Cross Finder"></a></figure>
                    <p class="c-list3__txt">無料集計ツール</p>
                    <h3 class="c-list3__tit">Cross Finder</h3>
                </li>
                <li>
                    <figure><a href=""><img src="/assets/img/top/service_icon6.png" alt="パネルについて"></a></figure>
                    <p class="c-list3__txt">リサーチにおける<br class="sp-only">重要性</p>
                    <h3 class="c-list3__tit">パネルについて</h3>
                </li>
                <li>
                    <figure><a href=""><img src="/assets/img/top/service_icon7.png" alt="U26"></a></figure>
                    <p class="c-list3__txt">20歳から26歳男子の会話から<br>ホンネが見えるコミュニティ</p>
                    <h3 class="c-list3__tit">U26</h3>
                </li>
                <li>
                    <figure class="c-list3__br2"><a href=""><img src="/assets/img/top/service_icon8.png" alt="iDOBATA KAIGI"></a></figure>
                    <p class="c-list3__txt">シニアのホンネに会える<br>Face to Faceコミュニティー</p>
                    <h3 class="c-list3__tit">iDOBATA KAIGI</h3>
                </li>
            </ul>
        </div>
        <ul class="c-bnt c-bnt__line">
            <li><a href="#"><span>アンケート調査から探す</span></a></li>
            <li><a href="#"><span>ソリューションから探す</span></a></li>
        </ul>
    </div>
</div>
<div class="p-top3">
    <div class="l-container">
        <h2 class="c-title1"><span class="c-title1__en">Future Marketing</span><span class="c-title1__jp">宣伝会議×Cross Marketing</span></h2>
        <ul class="c-list1">
            <li>
                <figure>
                    <img src="/assets/img/top/img1.png" alt="社内外のコミュニケーションにSDGsを活用する住友化学【前編】SDGsは長年の取り組みを可視化するチャンス">
                    <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">社内外のコミュニケーションにSDGsを活用する住友化学【前編】SDGsは長年の取り組みを可視化するチャンス</a>
                    </h3>
                    <p class="c-list1__text">
                        住友化学はもともと、銅の製錬の際に生じる有毒な排出ガスから肥料を製造し、煙害という環境問題を克服しながら、食糧の増産への...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img2.png" alt="伝え続けることで企業も社会も変えていくユニリーバ【後編】SDGs時代に求められるのはパーパスを伝えること">
                    <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">伝え続けることで企業も社会も変えていくユニリーバ【後編】SDGs時代に求められるのはパーパスを伝えること</a>
                    </h3>
                    <p class="c-list1__text">
                        SDGs先進企業と呼ばれるようになったユニリーバ。近年は消費者の意識も変化しはじめているという。変化する社会、消費者に合...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img3.png" alt="「MOTIONGALLERY」が目指す「社会彫刻」の形　第3回　参加できると思わせる「余白」が共感を生む">
                    <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">「MOTIONGALLERY」が目指す「社会彫刻」の形　第3回　参加できると思わせる「余白」が共感を生む</a>
                    </h3>
                    <p class="c-list1__text">
                        前回、私たちはプレゼンターに対してコンサルティングを行っているとお話ししました。プレゼンターにとってコレクターを集める、...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img4.png" alt="「子育てシェア」が作る高齢化時代の地域コミュニティ　第3回 ユーザーとの接点を活かし、より良いサービスに">
                    <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">「子育てシェア」が作る高齢化時代の地域コミュニティ　第3回 ユーザーとの接点を活かし、より良いサービスに</a>
                    </h3>
                    <p class="c-list1__text">
                        ここまでお話ししてきた通り「子育てシェア」サービスは、子育て世帯のニーズをとらえ、たくさんのユーザーに利用頂いていますが...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img5.png" alt="所有から利用へ　自動車の乗り分けを実現する「Anyca」　第3回 シェアリングは自動車の販売を伸ばすきっかけにもなる">
                    <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">所有から利用へ　自動車の乗り分けを実現する「Anyca」　第3回 シェアリングは自動車の販売を伸ばすきっかけにもなる</a>
                    </h3>
                    <p class="c-list1__text">
                        自動車の「所有」から「利用」へのシフトが進むことで危惧されるのが「自動車が売れなくなるのではないか」ということです。実際...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img6.png" alt="ecbo cloakの荷物預け体験から生まれる新たな出会い　第3回　2025年には500都市でecbo cloakの体験提供を目指す">
                    <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">ecbo cloakの荷物預け体験から生まれる新たな出会い　第3回　2025年には500都市でecbo cloakの体験提供を目指す</a>
                    </h3>
                    <p class="c-list1__text">
                        起業のきっかけが外国人旅行者のコインロッカー探しだったこともあり、「安心」「安全」に加えて、どこの国の人にも使いやすいサ...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
        </ul>
        <ul class="c-bnt c-bnt__line">
            <li><a href="#"><span>宣伝会議×Cross Marketing一覧</span></a></li>
        </ul>
    </div>
</div>
<div class="p-top4">
    <div class="l-container">
        <h2 class="c-title1"><span class="c-title1__en">Marketing Column</span><span class="c-title1__jp">マーケティングコラム</span></h2>
        <ul class="c-list1">
            <li>
                <figure>
                    <img src="/assets/img/top/img7.png" alt="競争優位を保つ成功要因を見つけ出すための3C分析">
                    <figcaption class="c-list1__fig2">マーケティングコラム</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">競争優位を保つ成功要因を見つけ出すための3C分析</a>
                    </h3>
                    <p class="c-list1__text">
                        3C分析は顧客や競合、自社の現状を把握し、既存のビジネスを目指すべき方向に導くために活用される分析手法です。今回は3C分...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img8.png" alt="ネットが揺るがす中国の都市ランキング">
                    <figcaption class="c-list1__fig3">グローバルコラム</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">ネットが揺るがす中国の都市ランキング</a>
                    </h3>
                    <p class="c-list1__text">
                        中国では、11月11日は「独身の日」です。「ひとり」を連想させる「1」が4つ並んでいることにちなんで、1990年代に若者...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img9.png" alt="高齢者ビジネスに必須のシニアマーケティングの要諦">
                    <figcaption class="c-list1__fig2">マーケティングコラム</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">高齢者ビジネスに必須のシニアマーケティングの要諦</a>
                    </h3>
                    <p class="c-list1__text">
                        先進国の中でも一二を争うほどの高齢化社会である日本において、シニア層をターゲットにしたビジネスは大きく伸びるとの判断から...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img10.png" alt="オンライン調査とオフライン調査の有効活用">
                    <figcaption class="c-list1__fig2">マーケティングコラム</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">オンライン調査とオフライン調査の有効活用</a>
                    </h3>
                    <p class="c-list1__text">
                        昨今のインターネット事情は、私たちの生活に欠かせない重要な役割を担っています。各先進国への普及により、多種多様な情報が瞬...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img11.png" alt="【新しい時代のリサーチ 第7回-Part1】RDITに関するQ&amp;A">
                    <figcaption class="c-list1__fig4">新しい時代のリサーチ</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">【新しい時代のリサーチ 第7回-Part1】RDITに関するQ&amp;A</a>
                    </h3>
                    <p class="c-list1__text">
                        RDITに関するお問合せが増えてきましたので、今回、第7回のコラムでは、ご質問の多かった点についてお答えしたいと思います...
                    </p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
            <li>
                <figure>
                    <img src="/assets/img/top/img12.png" alt="データ分析は答えがない問題に答えを出すための羅針盤">
                    <figcaption class="c-list1__fig2">マーケティングコラム</figcaption>
                </figure>
                <div class="c-list1__content">
                    <h3 class="c-list1__title">
                        <a href="#">データ分析は答えがない問題に答えを出すための羅針盤</a>
                    </h3>
                    <p class="c-list1__text">インターネット通販の広がりや本格的なIoT時代を迎え、企業には大量のデータが入ってくるようになりました。しかし、こうした...</p>
                    <p class="c-list1__date">2019/03/01 8時間前</p>
                </div>
            </li>
        </ul>
        <ul class="c-bnt c-bnt__line">
            <li><a href="#"><span>コラム一覧</span></a></li>
        </ul>
    </div>
</div>
<div class="p-top5">
    <div class="l-container">
        <h2 class="c-title1"><span class="c-title1__en">Survey Report</span><span class="c-title1__jp">無料調査レポート</span></h2>
        <ul class="c-list5">
            <li>
                <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
                <div class="c-list5__content">
                    <p class="c-list5__sub">季節・イベント・時事</p>
                    <h3 class="c-list5__tit">日本人の年中行事に関する調査</h3>
                    <p class="c-list5__date">2019/03/01 8時間前</p>
                    <p class="c-list5__tag"><a href="#">#時事ネタ</a></p>
                </div>
            </li>
            <li>
                <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
                <div class="c-list5__content">
                    <p class="c-list5__sub">消費動向</p>
                    <h3 class="c-list5__tit">消費動向に関する定点調査（2018年10月度）</h3>
                    <p class="c-list5__date">2019/03/01 8時間前</p>
                    <p class="c-list5__tag"><a href="#">#消費</a>, <a href="#">#食生活</a>, <a href="#">#自動車</a></p>
                </div>
            </li>
            <li>
                <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
                <div class="c-list5__content">
                    <p class="c-list5__sub">IT・家電</p>
                    <h3 class="c-list5__tit">オンライン上の口コミ利用に関する実態調査</h3>
                    <p class="c-list5__date">2019/03/01 8時間前</p>
                    <p class="c-list5__tag"><a href="#">#消費</a>, <a href="#">#IT</a>, <a href="#">#SNS</a></p>
                </div>
            </li>
            <li>
                <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
                <div class="c-list5__content">
                    <p class="c-list5__sub">レジャー・旅行</p>
                    <h3 class="c-list5__tit">民泊に関する調査</h3>
                    <p class="c-list5__date">2019/03/01 8時間前</p>
                    <p class="c-list5__tag"><a href="#">#旅行</a></p>
                </div>
            </li>
            <li>
                <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
                <div class="c-list5__content">
                    <p class="c-list5__sub">消費動向</p>
                    <h3 class="c-list5__tit">消費動向に関する定点調査（2018年5月度）</h3>
                    <p class="c-list5__date">2019/03/01 8時間前</p>
                    <p class="c-list5__tag"><a href="#">#消費</a>, <a href="#">#食生活</a>, <a href="#">#自動車</a></p>
                </div>
            </li>
            <li>
                <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
                <div class="c-list5__content">
                    <p class="c-list5__sub">スポーツ</p>
                    <h3 class="c-list5__tit">eスポーツに関する調査</h3>
                    <p class="c-list5__date">2019/03/01 8時間前</p>
                    <p class="c-list5__tag"><a href="#">#IT</a>, <a href="#">#ゲーム</a></p>
                </div>
            </li>
        </ul>
        <ul class="c-bnt c-bnt__line">
            <li><a href="#"><span>無料調査レポート一覧</span></a></li>
        </ul>
    </div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>