<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi3__login</div>
<div>
    <div class="c-navi3__login">
        <form>
            <label>お客様ID(メールアドレス)</label>
            <input type="text" placeholder="半角英数">
            <label>パスワード</label>
            <input type="password" placeholder="半角英数・記号">
            <div class="c-submit"><input type="submit" value="ログインする"></div>
        </form>
        <p><a href="/">パスワードをお忘れの方はこちら</a></p>
        <p><a href="/">無料会員登録(ID・パスワードをお持ちでないお客様)</a></p>
    </div>
    <div class="c-dev-title2">c-navi3__logout</div>
    <div class="c-navi3__logout">
        <p class="c-navi3__logout__tit">お客様ID(メールアドレス)</p>
        <p class="c-navi3__logout__address">dummy@address.com</p>
        <p class="c-bnt2"><a href="#"><span>マイページ</span></a></p>
        <p class="c-bnt2 c-bnt2__color1"><a href="#"><span>ログアウトする</span></a></p>
    </div>
</div>