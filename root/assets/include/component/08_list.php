<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="l-container">
    <ul class="c-list1">
        <li>
            <figure>
                <img src="/assets/img/top/img1.png" alt="社内外のコミュニケーションにSDGsを活用する住友化学【前編】SDGsは長年の取り組みを可視化するチャンス">
                <figcaption class="c-list1__fig1">インサイトスコープ</figcaption>
            </figure>
            <div class="c-list1__content">
                <h3 class="c-list1__title">
                    <a href="#">社内外のコミュニケーションにSDGsを活用する住友化学【前編】SDGsは長年の取り組みを可視化するチャンス</a>
                </h3>
                <p class="c-list1__text">
                    住友化学はもともと、銅の製錬の際に生じる有毒な排出ガスから肥料を製造し、煙害という環境問題を克服しながら、食糧の増産への...
                </p>
                <p class="c-list1__date">2019/03/01 8時間前</p>
                <p class="c-list1__tag"><a href="google.com">#ブランディング</a>, <a href="#">#ソーシャルグッド</a></p>
            </div>
        </li>
        <li>
            <figure>
                <img src="/assets/img/top/img7.png" alt="競争優位を保つ成功要因を見つけ出すための3C分析">
                <figcaption class="c-list1__fig2">マーケティングコラム</figcaption>
            </figure>
            <div class="c-list1__content">
                <h3 class="c-list1__title">
                    <a href="#">競争優位を保つ成功要因を見つけ出すための3C分析</a>
                </h3>
                <p class="c-list1__text">
                    3C分析は顧客や競合、自社の現状を把握し、既存のビジネスを目指すべき方向に導くために活用される分析手法です。今回は3C分...
                </p>
                <p class="c-list1__date">2019/03/01 8時間前</p>
                <p class="c-list1__tag"><a href="#">#消費市場</a>, <a href="#">#市場調査</a></p>
            </div>
        </li>
        <li>
            <figure>
                <img src="/assets/img/top/img8.png" alt="ネットが揺るがす中国の都市ランキング">
                <figcaption class="c-list1__fig3">グローバルコラム</figcaption>
            </figure>
            <div class="c-list1__content">
                <h3 class="c-list1__title">
                    <a href="#">ネットが揺るがす中国の都市ランキング</a>
                </h3>
                <p class="c-list1__text">
                    中国では、11月11日は「独身の日」です。「ひとり」を連想させる「1」が4つ並んでいることにちなんで、1990年代に若者...
                </p>
                <p class="c-list1__date">2019/03/01 8時間前</p>
                <p class="c-list1__tag"><a href="#">#テクノロジー</a>, <a href="#">#生活</a>, <a href="#">#文化</a>, <a href="#">#経済成長</a>, <a href="#">#消費者行動</a>, <a href="#">#消費市場</a>, <a href="#">#携帯</a>, <a href="#">#スマートフォン</a></p>
            </div>
        </li>
        <li>
            <figure>
                <img src="/assets/img/top/img11.png" alt="【新しい時代のリサーチ 第7回-Part1】RDITに関するQ&A">
                <figcaption class="c-list1__fig4">新しい時代のリサーチ</figcaption>
            </figure>
            <div class="c-list1__content">
                <h3 class="c-list1__title">
                    <a href="#">【新しい時代のリサーチ 第7回-Part1】RDITに関するQ&A</a>
                </h3>
                <p class="c-list1__text">
                    RDITに関するお問合せが増えてきましたので、今回、第7回のコラムでは、ご質問の多かった点についてお答えしたいと思います...
                </p>
                <p class="c-list1__date">2019/03/01 8時間前</p>
                <p class="c-list1__tag"><a href="#">#テクノロジー</a>, <a href="#">#アンケート調査</a></p>
            </div>
        </li>
    </ul>
</div>

<div class="c-dev-title2">c-list2</div>
<div class="l-container">
    <table class="c-list2">
        <tr>
            <td>
                <figure><img src="/assets/img/top/feature_img1.png" alt="マーケティングリサーチ"></figure>
                <h3 class="c-list2__tit"><span>Marketing Research</span>マーケティングリサーチ</h3>
                <p class="c-list2__text">
                    「市場」の動向だけでなく、市場の中身、顧客や満足度に至るまでを調査。データや数値のみでは計れない潜在的なニーズも察知・予測し、明確な目的と正しい手法で調査を実施することで、正確なデータをビジネスに有効活用できます。
                </p>
            </td>
            <td>
                <figure><img src="/assets/img/top/feature_img2.png" alt="データマーケティング"></figure>
                <h3 class="c-list2__tit"><span>Data Marketing</span>データマーケティング</h3>
                <p class="c-list2__text">
                    「市場」の動向だけでなく、市場の中身、顧客や満足度に至るまでを調査。データや数値のみでは計れない潜在的なニーズも察知・予測し、明確な目的と正しい手法で調査を実施することで、正確なデータをビジネスに有効活用できます。
                </p>
            </td>
            <td>
                <figure><img src="/assets/img/top/feature_img3.png" alt="グローバルリサーチ"></figure>
                <h3 class="c-list2__tit"><span>Global Research</span>グローバルリサーチ</h3>
                <p class="c-list2__text">
                    クライアントの海外展開のニーズに応えるため、アジア市場を中心に海外拠点を展開。言語だけでなく、商慣習、法律、宗教など異なる市場環境に対応できるフィールドワーク＆リサーチネットワークを保有しています。
                </p>
            </td>
            <td>
                <figure><img src="/assets/img/top/feature_img4.png" alt="コンサルタンシー"></figure>
                <h3 class="c-list2__tit"><span>Consultancy</span>コンサルタンシー</h3>
                <p class="c-list2__text">
                    お客さまと密なコミュニケーションを図り信頼関係を築き、マーケティング課題を正確にヒアリング。お客さまの課題の背景にある市場状況、経営戦略、事業戦略、販売戦略などを理解した上で、最も最適なソリューションを提供します。
                </p>
            </td>
        </tr>
    </table>
</div>

<div class="c-dev-title2">c-list3</div>
<div style=" background: #f4f4f4; padding-top:50px; padding-bottom:50px;">
    <div class="l-container">
        <ul class="c-list3">
            <li>
                <figure><a href=""><img src="/assets/img/top/service_icon1.png" alt="QiQUMO"></a></figure>
                <p class="c-list3__txt">セルフ型<br class="sp-only">アンケートツール</p>
                <h3 class="c-list3__tit">QiQUMO</h3>
            </li>
            <li>
                <figure><a href=""><img src="/assets/img/top/service_icon2.png" alt="オフラインリサーチ"></a></figure>
                <p class="c-list3__txt">消費者の生活や<br class="sp-only">感情に密着できる</p>
                <h3 class="c-list3__tit">オフラインリサーチ</h3>
            </li>
            <li>
                <figure><a href=""><img src="/assets/img/top/service_icon3.png" alt="データ分析・解析"></a></figure>
                <p class="c-list3__txt">目的や用途、<br class="sp-only">手法に合わせた</p>
                <h3 class="c-list3__tit">データ分析・解析</h3>
            </li>
            <li>
                <figure><a href=""><img src="/assets/img/top/service_icon4.png" alt="学術調査"></a></figure>
                <p class="c-list3__txt">学術研究機関の研究者や<br class="sp-only">学生向け</p>
                <h3 class="c-list3__tit">学術調査</h3>
            </li>
            <li>
                <figure class="c-list3__br2"><a href=""><img src="/assets/img/top/service_icon5.png" alt="Cross Finder"></a></figure>
                <p class="c-list3__txt">無料集計ツール</p>
                <h3 class="c-list3__tit">Cross Finder</h3>
            </li>
            <li>
                <figure><a href=""><img src="/assets/img/top/service_icon6.png" alt="パネルについて"></a></figure>
                <p class="c-list3__txt">リサーチにおける<br class="sp-only">重要性</p>
                <h3 class="c-list3__tit">パネルについて</h3>
            </li>
            <li>
                <figure><a href=""><img src="/assets/img/top/service_icon7.png" alt="U26"></a></figure>
                <p class="c-list3__txt">20歳から26歳男子の会話から<br>ホンネが見えるコミュニティ</p>
                <h3 class="c-list3__tit">U26</h3>
            </li>
            <li>
                <figure class="c-list3__br2"><a href=""><img src="/assets/img/top/service_icon8.png" alt="iDOBATA KAIGI"></a></figure>
                <p class="c-list3__txt">シニアのホンネに会える<br>Face to Faceコミュニティー</p>
                <h3 class="c-list3__tit">iDOBATA KAIGI</h3>
            </li>
        </ul>
    </div>
</div>
<div class="c-dev-title2">c-list4</div>
<div style=" background: #f4f4f4; padding-top:50px; padding-bottom:50px;">
    <div class="l-container">
        <ul class="c-list4">
            <li>
                <div class="c-list4__img"><img src="/assets/img/top/service_img1.png" alt="マーケティング課題から探す"></div>
                <div class="c-list4__text">
                    <h3 class="c-list4__tit"><a href="#">マーケティング課題から探す</a></h3>
                    <p class="c-list4__txt">この文章はダミーのテキストです。実際の内容とは異なりますので予めご了承ください。この文章はダミーのテキストです。</p>
                </div>
            </li>
            <li>
                <div class="c-list4__img"><img src="/assets/img/top/service_img2.png" alt="マーケティングプロセスから探す"></div>
                <div class="c-list4__text">
                    <h3 class="c-list4__tit"><a href="#">マーケティングプロセスから探す</a></h3>
                    <p class="c-list4__txt">この文章はダミーのテキストです。実際の内容とは異なりますので予めご了承ください。この文章はダミーのテキストです。</p>
                </div>
            </li>
            <li>
                <div class="c-list4__img"><img src="/assets/img/top/service_img3.png" alt="初めてのネットリサーチ"></div>
                <div class="c-list4__text">
                    <h3 class="c-list4__tit"><a href="#">初めてのネットリサーチ</a></h3>
                    <p class="c-list4__txt">この文章はダミーのテキストです。実際の内容とは異なりますので予めご了承ください。この文章はダミーのテキストです。</p>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="c-dev-title2">c-list5</div>
<div class="l-container">
    <ul class="c-list5">
        <li>
            <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img1.png" alt=""></a></figure>
            <div class="c-list5__content">
                <p class="c-list5__sub">季節・イベント・時事</p>
                <h3 class="c-list5__tit">日本人の年中行事に関する調査</h3>
                <p class="c-list5__date">2019/03/01 8時間前</p>
                <p class="c-list5__tag"><a href="#">#時事ネタ</a></p>
            </div>
        </li>
        <li>
            <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img2.png" alt=""></a></figure>
            <div class="c-list5__content">
                <p class="c-list5__sub">消費動向</p>
                <h3 class="c-list5__tit">消費動向に関する定点調査（2018年10月度）</h3>
                <p class="c-list5__date">2019/03/01 8時間前</p>
                <p class="c-list5__tag"><a href="#">#消費</a>, <a href="#">#食生活</a>, <a href="#">#自動車</a></p>
            </div>
        </li>
        <li>
            <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img3.png" alt=""></a></figure>
            <div class="c-list5__content">
                <p class="c-list5__sub">IT・家電</p>
                <h3 class="c-list5__tit">オンライン上の口コミ利用に関する実態調査</h3>
                <p class="c-list5__date">2019/03/01 8時間前</p>
                <p class="c-list5__tag"><a href="#">#消費</a>, <a href="#">#IT</a>, <a href="#">#SNS</a></p>
            </div>
        </li>
        <li>
            <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img4.png" alt=""></a></figure>
            <div class="c-list5__content">
                <p class="c-list5__sub">レジャー・旅行</p>
                <h3 class="c-list5__tit">民泊に関する調査</h3>
                <p class="c-list5__date">2019/03/01 8時間前</p>
                <p class="c-list5__tag"><a href="#">#旅行</a></p>
            </div>
        </li>
        <li>
            <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img5.png" alt=""></a></figure>
            <div class="c-list5__content">
                <p class="c-list5__sub">消費動向</p>
                <h3 class="c-list5__tit">消費動向に関する定点調査（2018年5月度）</h3>
                <p class="c-list5__date">2019/03/01 8時間前</p>
                <p class="c-list5__tag"><a href="#">#消費</a>, <a href="#">#食生活</a>, <a href="#">#自動車</a></p>
            </div>
        </li>
        <li>
            <figure class="c-list5__img"><a href="#"><img src="/assets/img/top/survey_img6.png" alt=""></a></figure>
            <div class="c-list5__content">
                <p class="c-list5__sub">スポーツ</p>
                <h3 class="c-list5__tit">eスポーツに関する調査</h3>
                <p class="c-list5__date">2019/03/01 8時間前</p>
                <p class="c-list5__tag"><a href="#">#IT</a>, <a href="#">#ゲーム</a></p>
            </div>
        </li>
    </ul>
</div>
<div class="c-dev-title2">c-list6</div>
<div class="l-container2">
    <ul class="c-list6">
        <li>
            <p class="c-list6__img"><a href=""><img src="/assets/img/common/content_img1.png" alt="パネル"></a></p>
            <p class="c-list6__txt">パネル</p>
        </li>
        <li>
            <p class="c-list6__img"><a href=""><img src="/assets/img/common/content_img2.png" alt="スペシャリティパネル"></a></p>
            <p class="c-list6__txt">スペシャリティパネル</p>
        </li>
        <li>
            <p class="c-list6__img"><a href=""><img src="/assets/img/common/content_img3.png" alt="よくあるご質問"></a></p>
            <p class="c-list6__txt">よくあるご質問</p>
        </li>
    </ul>
</div>
<div class="c-dev-title2">c-list7</div>
<div class="l-container2">
    <ul class="c-list7">
        <li>
            <a href=""><img src="/assets/img/common/column_img1.png" alt="ネットリサーチにおけるスマホとPCの差異と今後の展望"></a>
            <p class="c-list7__txt1">ネットリサーチにおけるスマホとPCの差異と今後の展望</p>
            <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
        </li>
        <li>
            <a href=""><img src="/assets/img/common/column_img2.png" alt="ネットリサーチは「パネル」と「リサーチャー」によって質が決まる"></a>
            <p class="c-list7__txt1">ネットリサーチは「パネル」と「リサーチャー」によって質が決まる</p>
            <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
        </li>
        <li>
            <a href=""><img src="/assets/img/common/column_img3.png" alt="スマートフォン利用者を意識した調査をしよう"></a>
            <p class="c-list7__txt1">スマートフォン利用者を意識した調査をしよう</p>
            <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
        </li>
        <li>
            <a href=""><img src="/assets/img/common/column_img4.png" alt="あらためて知っておきたい ネットリサーチのメリットとデメリット"></a>
            <p class="c-list7__txt1">あらためて知っておきたい ネットリサーチのメリットとデメリット</p>
            <p class="c-list7__txt2"><a href="#">マーケティングコラム</a></p>
        </li>
    </ul>
</div>
<div class="c-dev-title2">c-list8</div>
<div class="l-container2">
    <ul class="c-list8">
        <li>
            <a href="#1"><img src="/assets/img/netresearch/icon1.png" alt="アクティブパネル420万人以上"></a>
            <p>アクティブパネル<br>420万人以上</p>
        </li>
        <li>
            <a href="#2"><img src="/assets/img/netresearch/icon2.png" alt="柔軟な調査画面設計"></a>
            <p>柔軟な<br>調査画面設計</p>
        </li>
        <li>
            <a href="#3"><img src="/assets/img/netresearch/icon3.png" alt="無料集計ツール"></a>
            <p>無料<br>集計ツール</p>
        </li>
    </ul>
</div>

<div class="c-dev-title2">c-list9</div>
<div class="l-container2">
    <div class="c-list9">
        <div class="c-list9__box">
            <div class="c-list9__fll">
                <div class="c-list9__tt">
                    <figure><img src="/assets/img/netresearch/check1.png" alt="Check 01"></figure>
                    <h3>処理速度を<br>大幅改善！</h3>
                </div>
                <p class="c-list9__text">
                    データ取り込み速度を大幅に改善。
                    <br>集計表やグラフ作成も、さらに早く美しく。
                </p>
            </div>
            <div class="c-list9__flr">
                <figure><img src="/assets/img/netresearch/img1.png" alt="処理速度を大幅改善！"></figure>
                <h4>データ読み込み速度の比較</h4>
                <p class="c-list9__note">
                    ※データは1,000サンプル、200カラムでの比較参考値です。
                </p>
            </div>
        </div>
        <div class="c-list9__box">
            <div class="c-list9__fll">
                <div class="c-list9__tt">
                    <figure><img src="/assets/img/netresearch/check2.png" alt="Check 02"></figure>
                    <h3>マニュアルなしで<br>ラクラク集計！</h3>
                </div>
                <p class="c-list9__text">
                    初めての方でも分かりやすい画面で、面倒な設定も不要！
                    <br>さらに使いやすく生まれ変わりました。
                </p>
            </div>
            <div class="c-list9__flr">
                <figure><img src="/assets/img/netresearch/img2.png" alt="マニュアルなしでラクラク集計！"></figure>
            </div>
        </div>
        <div class="c-list9__box">
            <div class="c-list9__fll">
                <div class="c-list9__tt">
                    <figure><img src="/assets/img/netresearch/check3.png" alt="Check 03"></figure>
                    <h3>検定・多変量解析<br>機能の追加！</h3>
                </div>
                <p class="c-list9__text">
                    検定・多変量解析機能を標準装備。
                    <br>t検定やz検定、コレスポンデンス分析などがこれ1本で実現できます。
                </p>
            </div>
            <div class="c-list9__flr">
                <figure><img src="/assets/img/netresearch/img2.png" alt="検定・多変量解析機能の追加！"></figure>
            </div>
        </div>
    </div>
</div>